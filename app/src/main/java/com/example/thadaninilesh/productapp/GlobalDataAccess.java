package com.example.thadaninilesh.productapp;

import android.app.Activity;

/**
 * Created by thada on 19-05-2016.
 */
public class GlobalDataAccess extends Activity {

    public String name, model, type, serialId, user_id, data;
    public String getUrl(){
        String url = "http://192.168.0.6/ProductApp/php/";//getString(R.string.url);
        return url;
    }

    public GlobalDataAccess(){

    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public GlobalDataAccess(String name, String model, String type, String serialId, String user_id) {
        this.model = model;
        this.name = name;
        this.serialId = serialId;
        this.type = type;
        this.user_id = user_id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialId() {
        return serialId;
    }

    public void setSerialId(String serialId) {
        this.serialId = serialId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
