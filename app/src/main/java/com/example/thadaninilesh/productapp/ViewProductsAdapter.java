package com.example.thadaninilesh.productapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Fragment;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by thada on 24-05-2016.
 */
public class ViewProductsAdapter extends ArrayAdapter {
    List list = new ArrayList();
    private int layout;
    public ViewProductsAdapter(Context context, int resource){
        super(context, resource);
        layout = resource;
    }
    public void add(ViewProductsList object){
        super.add(object);
        list.add(object);
    }
    public int getCount(){
        return list.size();
    }
    public Object getItem(int position){
        return list.get(position);
    }
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View row;
        row = convertView;
        final ViewProductsHolder viewProductsHolder;
        if(row==null){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout,parent,false);
            viewProductsHolder = new ViewProductsHolder();
            viewProductsHolder.tx_name = (TextView)row.findViewById(R.id.tx_name);
            viewProductsHolder.tx_model = (TextView)row.findViewById(R.id.tx_model);
            viewProductsHolder.tx_type = (TextView)row.findViewById(R.id.tx_type);
            viewProductsHolder.tx_serialId = (TextView)row.findViewById(R.id.tx_serialId);
            viewProductsHolder.tx_added_on = (TextView)row.findViewById(R.id.tx_added_on);
            viewProductsHolder.button = (Button)row.findViewById(R.id.list_item_btn);
            row.setTag(viewProductsHolder);
        }
        else{
            viewProductsHolder = (ViewProductsHolder) row.getTag();
        }

        final ViewProductsList viewProductsList = (ViewProductsList) this.getItem(position);
        viewProductsHolder.tx_name.setText("Name: "+viewProductsList.getName());
        viewProductsHolder.tx_model.setText("Model: "+viewProductsList.getModel());
        viewProductsHolder.tx_type.setText("Type: "+viewProductsList.getType());
        viewProductsHolder.tx_serialId.setText("Serial Id: "+viewProductsList.getSerialId());
        viewProductsHolder.tx_added_on.setText("Added On: "+viewProductsList.getAdded_on());
        viewProductsHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewSelectedProductFragment viewSelectedProductFragment = new ViewSelectedProductFragment();
                Bundle args = new Bundle();
                args.putString("name",viewProductsList.getName());
                args.putString("model",viewProductsList.getModel());
                args.putString("type",viewProductsList.getType());
                args.putString("serialId",viewProductsList.getSerialId());
                args.putString("user_id",viewProductsList.getUser_id());
                args.putString("added_on",viewProductsList.getAdded_on());
                viewSelectedProductFragment.setArguments(args);
                final Context context = parent.getContext();
                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, viewSelectedProductFragment).addToBackStack("viewSelectedProduct").commit();
            }
        });
        return row;
    }

    static class ViewProductsHolder{
        TextView tx_name, tx_model, tx_type, tx_serialId, tx_added_on;
        Button button;
    }
}
