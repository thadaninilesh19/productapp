package com.example.thadaninilesh.productapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BackgroundUserRegisterTask extends AsyncTask<String,Void,String> {
    Activity ctx;
    AlertDialog alertDialog;
    ProgressDialog progressDialog = null;
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedpreferences;
    BackgroundUserRegisterTask(Activity ctx){
        this.ctx = ctx;
    }
    String method="";
    String path="";
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        path = ctx.getString(R.string.url);
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setTitle("Signing Up");
        progressDialog.setMessage("Checking your details!");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        String result="";
        //Toast.makeText(ctx,path,Toast.LENGTH_LONG).show();

        path = path + "register.php";
        String name, email, username, password;
        name = params[0];
        email = params[1];
        username = params[2];
        password = params[3];
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String created_at = sdf.format(c.getTime());
        Log.d("register", username + " " + password);
        try {
            URL url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            String data = URLEncoder.encode("user_name","UTF-8") +"="+URLEncoder.encode(name,"UTF-8")+"&"+
                    URLEncoder.encode("password","UTF-8") +"="+URLEncoder.encode(password,"UTF-8")+"&"+
                    URLEncoder.encode("email","UTF-8") +"="+URLEncoder.encode(email,"UTF-8")+"&"+
                    URLEncoder.encode("username","UTF-8") +"="+URLEncoder.encode(username,"UTF-8")+"&"+
                    URLEncoder.encode("created_at","UTF-8") +"="+URLEncoder.encode(created_at,"UTF-8");

            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            os.close();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        Log.d("register",response);
        return response;
    }



    @Override
    protected void onProgressUpdate(Void... values){
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        if(progressDialog!=null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            progressDialog= null;
        }
        if(result.equals("duplicate")){
            Toast.makeText(ctx,"Duplicate entry found! Please try again with different username or email id",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, RegisterActivity.class));
        }
        else if(result.equals("Registeration successful! Login to continue")){
            Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, LoginActivity.class));
            ctx.finish();
        }
        else{
            Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, RegisterActivity.class));
        }

    }
}