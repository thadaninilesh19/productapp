package com.example.thadaninilesh.productapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.EGLDisplay;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddComplainProductFragment extends Fragment {

    TextView tx_complainType, tx_error, tx_serialId;
    EditText et_type, et_msg;
    Button complain;
    Spinner spinner;
    CheckBox checkBox;
    String name, model, type, serial;
    private ArrayList<Product> productList = new ArrayList<Product>();

    public AddComplainProductFragment() {
        // Required empty public constructor
    }
    JSONObject jsonObject;
    JSONArray jsonArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_complain_product, container, false);
        tx_error = (TextView)view.findViewById(R.id.errorMessage);
        tx_serialId = (TextView)view.findViewById(R.id.serialID);
        et_type = (EditText)view.findViewById(R.id.complainType);
        et_msg = (EditText)view.findViewById(R.id.message);
        spinner = (Spinner)view.findViewById(R.id.productList);
        checkBox = (CheckBox)view.findViewById(R.id.checkbox);
        new BackgroundViewSpinnerProductTask().execute();
        complain = (Button)view.findViewById(R.id.submitComplain);
        Bundle data = this.getArguments();
        name = data.getString("name");
        model = data.getString("model");
        type = data.getString("type");
        serial = data.getString("serial");
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked()){
                    et_msg.setVisibility(EditText.VISIBLE);
                }
                else{
                    et_msg.setVisibility(EditText.GONE);
                }
            }
        });
        return view;
    }

    private void populateSpinner(){
        List<String> lables = new ArrayList<String>();

        for (int i = 0; i < productList.size(); i++) {
            lables.add(productList.get(i).getName());
        }

        // Creating adapter for spinner
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(spinnerAdapter);
        for(int i=0; i<productList.size();i++){
            if(productList.get(i).getSerial().equals(serial)){
                Toast.makeText(getActivity(),serial,Toast.LENGTH_LONG).show();
                spinner.setSelection(i);
            }
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                tx_serialId.setText("Serial ID: "+productList.get(position).getSerial());
                complain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String productName, message, complainType, serialid;
                        productName = spinner.getSelectedItem().toString();
                        message = et_msg.getText().toString().trim();
                        complainType = et_type.getText().toString().trim();
                        serialid = productList.get(position).getSerial();
                        if(complainType.isEmpty()){
                            tx_error.setText("Complain type field is mandatory");
                        }
                        else{
                            if(message.isEmpty()){
                                message = "--";
                            }
                            new BackgroundAddComplainTask().execute(productName, message, complainType, serialid);
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private class BackgroundViewSpinnerProductTask extends AsyncTask<Void, Void, String>{
        ProgressDialog progressDialog = null;

        String method="";
        String path="";
        String user_id;
        public static final String MyPREFERENCES = "ProductApp" ;
        SharedPreferences sharedPreferences;
        Activity ctx;
        BackgroundViewSpinnerProductTask(){
            ctx = getActivity();
        }
        @Override
        protected void onPreExecute(){
            path = ctx.getString(R.string.url);
            sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            user_id = sharedPreferences.getString("user_id", "");
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading products");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            String response = "";
            String result="";
            path = path + "getProduct.php";
            try {
                URL url = new URL(path);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                String data = URLEncoder.encode("id","UTF-8") +"="+URLEncoder.encode(user_id,"UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }

                sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("products", response);
                editor.commit();

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result!="empty"){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject!=null){
                        JSONArray products = jsonObject.getJSONArray("server_response");
                        for (int i = 0; i < products.length(); i++) {
                            JSONObject productsObject = (JSONObject) products.get(i);
                            //Log.d("productdetails>",productsObject.getString("model")+" "+ productsObject.getString("name")+" "+productsObject.getString("serialid")+" "+productsObject.getString("type")+" ");
                            Product pro = new Product(productsObject.getString("model"), productsObject.getString("name"),productsObject.getString("serialid"),productsObject.getString("type"));
                            productList.add(pro);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                progressDialog= null;
            }
            populateSpinner();
        }
    }

    private class Product{
        public String name, model, type, serial;

        public Product(String model, String name, String serial, String type) {
            this.model = model;
            this.name = name;
            this.serial = serial;
            this.type = type;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    private class BackgroundAddComplainTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog = null;
        public static final String MyPREFERENCES = "ProductApp" ;
        SharedPreferences sharedPreferences;
        Activity ctx;
        String path;
        String user_id;
        public BackgroundAddComplainTask() {
            ctx = getActivity();
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            path = ctx.getString(R.string.url);
            sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            user_id = sharedPreferences.getString("user_id", "");
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setTitle("Submitting Complain");
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            path = ctx.getString(R.string.url);
            String response = "";
            String productName, message, complainType, serialid;
            productName = params[0];
            message = params[1];
            complainType = params[2];
            serialid = params[3];
            path = path+"addComplain.php";
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String added_on = sdf.format(c.getTime());
            try {
                URL url = new URL(path);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                String data = URLEncoder.encode("productName","UTF-8") +"="+URLEncoder.encode(productName,"UTF-8")+"&"+
                        URLEncoder.encode("complainType","UTF-8") +"="+URLEncoder.encode(complainType,"UTF-8")+"&"+
                        URLEncoder.encode("message","UTF-8") +"="+URLEncoder.encode(message,"UTF-8")+"&"+
                        URLEncoder.encode("serialid","UTF-8") +"="+URLEncoder.encode(serialid,"UTF-8")+"&"+
                        URLEncoder.encode("user_id","UTF-8") +"="+URLEncoder.encode(user_id,"UTF-8")+"&"+
                        URLEncoder.encode("added_on","UTF-8") +"="+URLEncoder.encode(added_on,"UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result.equals("success")){
                Toast.makeText(getActivity(),"Complain submitted!",Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(ctx,"Error occured!Please try again later...",Toast.LENGTH_LONG).show();
            }
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                progressDialog = null;
            }
        }
    }


}
