package com.example.thadaninilesh.productapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class BackgroundUserLoginTask extends AsyncTask<String,Void,String> {
    Activity ctx;
    AlertDialog alertDialog;
    ProgressDialog progressDialog = null;
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedpreferences;
    BackgroundUserLoginTask(Activity ctx){
        this.ctx = ctx;
        progressDialog = new ProgressDialog(ctx);
    }
    String method="";
    String path="";
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        path = ctx.getString(R.string.url);
        progressDialog.setTitle("Signing In");
        progressDialog.setMessage("Validating your credentials!");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        String result="";
        //Toast.makeText(ctx,path,Toast.LENGTH_LONG).show();

        path = path + "log.php";
        String username, password;
        username = params[0];
        password = params[1];
        try {
            Log.d("response", response+"before in try");
            URL url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" +
                    URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }

            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
            if(response.equals("Invalid")){
                return response;
            }
            else {
                sharedpreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("username", username);
                response = response.replaceAll("\"","");
                response = response.replaceAll("\\[", "").replaceAll("\\]","");
                String[] resultingArray = response.split(",");
                for (int i = 0; i < 3; i++) {
                    result += resultingArray[i] + " ";
                    Log.d("jsonArray", result);
                }
                response = "success";

                editor.putString("email", resultingArray[0]);
                editor.putString("user_id", resultingArray[2]);
                editor.putString("name", resultingArray[1]);
                editor.commit();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }



    @Override
    protected void onProgressUpdate(Void... values){
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);

        if(progressDialog!=null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
        ctx.finish();

        if(result.equals("Invalid")){
            Toast.makeText(ctx,result+" credentials! Please try again",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, LoginActivity.class));
        }
        else if(result.equals("success")){
            Toast.makeText(ctx,"Login Successful",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }
        else{
            Toast.makeText(ctx,"Error occured! Please try again.",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, LoginActivity.class));
        }
    }
}
