package com.example.thadaninilesh.productapp;

/**
 * Created by thada on 24-05-2016.
 */
public class ViewProductsList {
    private String name, type, model, serialId, username, added_on, user_id;

    public ViewProductsList(String model, String name, String serialId, String type, String username, String added_on, String user_id) {
        this.model = model;
        this.name = name;
        this.serialId = serialId;
        this.type = type;
        this.username = username;
        this.added_on = added_on;
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialId() {
        return serialId;
    }

    public void setSerialId(String serialId) {
        this.serialId = serialId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
