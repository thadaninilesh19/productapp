package com.example.thadaninilesh.productapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BackgroundForgetPasswordTask extends AsyncTask<String,Void,String> {
    Context ctx;
    AlertDialog alertDialog;
    ProgressDialog progressDialog = null;
    BackgroundForgetPasswordTask(Context ctx){
        this.ctx = ctx;
    }
    String path="";
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        path = ctx.getString(R.string.url);
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("You'll receive an email soon on your registered e-mail address");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";

        path = "http://nishasajnani.esy.es/initiateForgetPassword.php";//path + "forgetPassword.php";
        String email;
        email = params[0];
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String requested_at = sdf.format(c.getTime());
        try {
            URL url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&" +
                    URLEncoder.encode("requested_at", "UTF-8") + "=" + URLEncoder.encode(requested_at, "UTF-8");
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }
            Log.d("response", response);

            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }



    @Override
    protected void onProgressUpdate(Void... values){

    }

    @Override
    protected void onPostExecute(String result){
        if(progressDialog!=null){
            if(progressDialog.isShowing() && progressDialog!=null){
                progressDialog.dismiss();
            }
            progressDialog= null;
        }
        if(result.equals("success")){
            Toast.makeText(ctx,"You will receive an email shortly",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, LoginActivity.class));
        }
        else if(result.equals("error")){
            Toast.makeText(ctx,"Error occured! Please try again later",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, ForgetPasswordActivity.class));
        }
        else if (result.equals("invalid")){
            Toast.makeText(ctx,"Invalid E-mail Address",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, ForgetPasswordActivity.class));
        }
    }
}