package com.example.thadaninilesh.productapp;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {


    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    UserDetails userDetails;
    Button changePass;
    TextView tv_error;
    EditText et_oldPass, et_newPass, et_newPassConfirm;
    LinearLayout ll_view, ll_edit;
    String username;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        username = sharedPreferences.getString("username", null);
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        et_oldPass = (EditText)view.findViewById(R.id.oldPassword);
        et_newPass = (EditText)view.findViewById(R.id.newPassword);
        et_newPassConfirm = (EditText)view.findViewById(R.id.newPasswordConfirm);
        tv_error = (TextView)view.findViewById(R.id.errorMessage);
        changePass = (Button)view.findViewById(R.id.changePassword);
        ActionBar actionBar = getActivity().getActionBar();
        //actionBar.setTitle("Change Password");
        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPass, newPass, newPassConf;
                oldPass = et_oldPass.getText().toString();
                newPass = et_newPass.getText().toString();
                newPassConf = et_newPassConfirm.getText().toString();
                if(oldPass.isEmpty() || newPass.isEmpty() || newPassConf.isEmpty()){
                    tv_error.setText("All fields are compulsory!");
                }
                else{
                    if(newPass.equals(newPassConf)){
                        if(oldPass.equals(newPassConf)){
                            tv_error.setText("Old and new password cannot be same");
                        }
                        else{
                            Log.d("new-old",newPass+" "+oldPass+" "+newPassConf+" "+username);
                            userDetails.updateUserPassword(username.trim().toString(), oldPass.trim().toString(), newPass.trim().toString());
                        }
                    }
                    else{
                        tv_error.setText("New password do not match!");
                        Log.d("new-old",newPass+" "+oldPass+" "+newPassConf);
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails)activity;
        }
        catch (Exception e){}
    }

    public interface UserDetails{
        public void updateUserPassword(String username, String old_password, String new_password);
    }

}
