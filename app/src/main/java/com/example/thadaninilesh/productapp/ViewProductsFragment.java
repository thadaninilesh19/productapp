package com.example.thadaninilesh.productapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class ViewProductsFragment extends Fragment {
    String jsonString;
    UserDetails userDetails;
    ListView listView;
    Button addProduct;
    String json;
    String name, model, type, serialId, user_name, added_on, user_id;
    ViewProductsAdapter viewProductsAdapter;
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    JSONObject jsonObject;
    JSONArray jsonArray;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_products, container, false);
        listView = (ListView)view.findViewById(R.id.list);
        addProduct = (Button)view.findViewById(R.id.addProduct);
        BackgroundViewProductsTask backgroundViewProductsTask = new BackgroundViewProductsTask();
        backgroundViewProductsTask.execute();

        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.changeActionTitle("Add Product");
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new AddProductFragment()).addToBackStack("addProduct").commit();
            }
        });

        return view;
    }

    public void populateProductList(String jsonString){
        viewProductsAdapter = new ViewProductsAdapter(this.getActivity(), R.layout.row_layout);
        listView.setAdapter(viewProductsAdapter);
        viewProductsAdapter.notifyDataSetChanged();
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        jsonString = sharedPreferences.getString("products", "");

        if(jsonString.isEmpty()){
            Toast.makeText(getActivity(),"No Products found",Toast.LENGTH_LONG).show();
            this.startActivity(new Intent(getActivity(), MainActivity.class));
        }
        try {
            jsonObject = new JSONObject(jsonString);
            jsonArray = jsonObject.getJSONArray("server_response");
            int count = 0;

            while (count<jsonArray.length()){
                JSONObject JO = jsonArray.getJSONObject(count);
                if(JO.getString("name").isEmpty()){
                    startActivity(new Intent(this.getActivity(), LoginActivity.class));
                }
                name = JO.getString("name");
                model = JO.getString("model");
                type = JO.getString("type");
                serialId = JO.getString("serialid");
                user_name = JO.getString("user_name");
                added_on = JO.getString("added_on");
                user_id = JO.getString("user_id");
                ViewProductsList viewProductsList = new ViewProductsList(model, name, serialId, type, user_name, added_on, user_id);
                viewProductsAdapter.add(viewProductsList);
                count++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails) activity;
        }
        catch (Exception e){}
    }

    private class BackgroundViewProductsTask extends AsyncTask<String,Void,String> {
        Activity ctx;
        ProgressDialog progressDialog = null;
        public static final String MyPREFERENCES = "ProductApp" ;
        SharedPreferences sharedPreferences;
        String method="";
        String path="";
        String user_id;

        public BackgroundViewProductsTask() {
            this.ctx = getActivity();
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            path = ctx.getString(R.string.url);
            sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            user_id = sharedPreferences.getString("user_id", "");
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setTitle("Loading products...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            String result="";
            path = path + "getProduct.php";
            try {
                URL url = new URL(path);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                String data = URLEncoder.encode("id","UTF-8") +"="+URLEncoder.encode(user_id,"UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }

                sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("products", response);
                editor.commit();

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return response;
        }



        @Override
        protected void onProgressUpdate(Void... values){
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(result!="empty"){
                populateProductList(result);
            }
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                progressDialog= null;
            }
        }
    }

    public interface UserDetails{
        public void viewProduct(String id);
        public void changeActionTitle(String title);
    }

}
