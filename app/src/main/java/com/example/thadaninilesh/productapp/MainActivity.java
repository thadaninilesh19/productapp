package com.example.thadaninilesh.productapp;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UserDetailsFragment.UserDetails, ChangePasswordFragment.UserDetails, AddProductFragment.UserDetails, ViewSelectedProductFragment.UserDetails, DashboardFragment.UserDetails, ViewProductsFragment.UserDetails {

    @Override
    public void viewProduct(String id) {

    }

    @Override
    public void changeActionTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void changeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void updateUser(String name, String new_name, String email, String new_email, String username, String new_username) {
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            BackgroundUserEditTask backgroundUserEditTask = new BackgroundUserEditTask(this);
            backgroundUserEditTask.execute(name, new_name, email, new_email, username, new_username);
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void updateUserPassword(String username, String old_password, String new_password) {
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            BackgroundUserChangePasswordTask backgroundUserChangePasswordTask = new BackgroundUserChangePasswordTask(this);
            backgroundUserChangePasswordTask.execute(username, old_password, new_password);
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void addProduct(String name, String type, String model, String serialId) {
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            BackgroundAddProductTask backgroundAddProductTask = new BackgroundAddProductTask(this);
            backgroundAddProductTask.execute(name, model, type, serialId);
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void deleteProduct(String serialId) {
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            BackgroundDeleteProductTask backgroundDeleteProductTask = new BackgroundDeleteProductTask(this);
            backgroundDeleteProductTask.execute(serialId);
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void updateProduct(String name, String model, String type, String serialId) {
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            BackgroundEditProductTask backgroundEditProductTask = new BackgroundEditProductTask(this);
            backgroundEditProductTask.execute(name, model, type, serialId);
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new DashboardFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        ActionBar actionBar = getActionBar();
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();
        if (id == R.id.nav_dashboard) {
            getSupportActionBar().setTitle("User Dashboard");
            fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new DashboardFragment()).addToBackStack("dashboard").commit();
        } else if (id == R.id.nav_addProduct) {
            getSupportActionBar().setTitle("Add Product");
            fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new AddProductFragment()).addToBackStack("addProduct").commit();
        } else if (id == R.id.nav_viewProducts) {
            CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent) {
                getSupportActionBar().setTitle("View Products");
                Fragment viewProductsFragment = new ViewProductsFragment();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_activity_frame, viewProductsFragment).addToBackStack("viewProducts").commit();
            }
            else{
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        }
        else if (id == R.id.nav_edit_details) {
            getSupportActionBar().setTitle("Edit Details");
            fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new UserDetailsFragment()).addToBackStack("editDetails").commit();
        } else if (id == R.id.nav_logout) {
            CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent) {
                getSupportActionBar().setTitle("Logout");
                BackgroundUserLogoutTask backgroundUserLogoutTask = new BackgroundUserLogoutTask(MainActivity.this);
                backgroundUserLogoutTask.execute();
             //   finishAffinity();
            }
            else{
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_change_pass) {
            CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if(isInternetPresent) {
                getSupportActionBar().setTitle("Change Password");
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new ChangePasswordFragment()).addToBackStack("changePassword").commit();
            }
            else{
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        }
        else if (id == R.id.nav_addComplain) {
            CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                getSupportActionBar().setTitle("Add Complain");
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new AddComplainFragment()).addToBackStack("complainFragment").commit();
            } else {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        }
        else if (id == R.id.nav_viewComplain) {
            CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                getSupportActionBar().setTitle("View Complains");
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new ViewComplainFragment()).addToBackStack("complainFragment").commit();
            } else {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
