package com.example.thadaninilesh.productapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ForgetPasswordActivity extends AppCompatActivity {
    String email;
    EditText et_email;
    Button forgetPassword;
    TextView tv_error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        et_email = (EditText)findViewById(R.id.email);
        forgetPassword = (Button)findViewById(R.id.forgetPassword);
        tv_error = (TextView)findViewById(R.id.errorMessage);
    }

    public void login(View view){
        startActivity(new Intent(ForgetPasswordActivity.this, LoginActivity.class));
        finish();
    }

    public void forgetPass(View view){
        String email = et_email.getText().toString();
        if(email.isEmpty()){
            tv_error.setText("Email address field is empty");
        }
        else{
            BackgroundForgetPasswordTask backgroundForgetPasswordTask = new BackgroundForgetPasswordTask(this);
            backgroundForgetPasswordTask.execute(email);
            finish();
        }
    }

    @Override
    public void onBackPressed(){
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            startActivity(new Intent(ForgetPasswordActivity.this, LoginActivity.class));
            finish();
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

}
