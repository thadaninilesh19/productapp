package com.example.thadaninilesh.productapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    EditText et_name, et_username, et_email, et_password, et_cpassword;
    String name, email, username, password, cpassword;
    TextView error;
    Button login,register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        et_name = (EditText)findViewById(R.id.name);
        et_email = (EditText)findViewById(R.id.email);
        et_username = (EditText)findViewById(R.id.username);
        et_password = (EditText)findViewById(R.id.password);
        et_cpassword = (EditText)findViewById(R.id.confirmPassword);
        error = (TextView)findViewById(R.id.errorMessage);
        login = (Button)findViewById(R.id.login);
        register = (Button)findViewById(R.id.register);
    }

    public void userLogin(View view){
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    public void userRegister(View view){
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            name = et_name.getText().toString().trim();
            email = et_email.getText().toString().trim();
            username = et_username.getText().toString().trim();
            password = et_password.getText().toString().trim();
            cpassword = et_cpassword.getText().toString().trim();
            if(name.isEmpty() || email.isEmpty() || username.isEmpty() || password.isEmpty() || cpassword.isEmpty()){
                error.setText("All fields are mandatory!");
                if(password!=cpassword){
                    error.append(" Password do not match.");
                }
            }
            else if(!(password.equals(cpassword))){
                error.setText("Password do not match."+cpassword+" "+password);
            }
            else{
                BackgroundUserRegisterTask backgroundUserRegisterTask = new BackgroundUserRegisterTask(this);
                backgroundUserRegisterTask.execute(name, email, username, password);
                //finish();
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

}
