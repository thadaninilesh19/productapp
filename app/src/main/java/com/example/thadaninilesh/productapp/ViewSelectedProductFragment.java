package com.example.thadaninilesh.productapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


public class ViewSelectedProductFragment extends Fragment {
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    TextView tx_productName, tx_model, tx_type, tx_serialID, tx_added_on, tx_productTitle, tx_error, tx_serialEdit;
    EditText et_name, et_model, et_type;
    Button edit, delete, extendWarranty, editConfirm, complainButton;
    LinearLayout viewer, editer;
    UserDetails userDetails;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_selected_product, container, false);
        final String name, model, type, serial, user_id, added_on;
        tx_productName = (TextView)view.findViewById(R.id.productName);
        tx_model = (TextView)view.findViewById(R.id.model);
        tx_type = (TextView)view.findViewById(R.id.type);
        tx_serialID = (TextView)view.findViewById(R.id.serialId);
        tx_added_on = (TextView)view.findViewById(R.id.added_on);
        tx_productTitle = (TextView)view.findViewById(R.id.productTitle);
        tx_error = (TextView)view.findViewById(R.id.errorMessage);
        tx_serialEdit = (TextView)view.findViewById(R.id.serialEdit);
        et_name = (EditText)view.findViewById(R.id.nameEdit);
        et_model = (EditText)view.findViewById(R.id.modelEdit);
        et_type = (EditText)view.findViewById(R.id.typeEdit);
        edit = (Button)view.findViewById(R.id.editProduct);
        delete = (Button)view.findViewById(R.id.delete);
        extendWarranty = (Button)view.findViewById(R.id.extendWarranty);
        complainButton = (Button)view.findViewById(R.id.complain);
        viewer = (LinearLayout)view.findViewById(R.id.view);
        editer = (LinearLayout)view.findViewById(R.id.edit);
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String current_userId = sharedPreferences.getString("user_id", "");
        Bundle data = this.getArguments();
        name = data.getString("name");
        model = data.getString("model");
        type = data.getString("type");
        serial = data.getString("serialId");
        user_id = data.getString("user_id");
        added_on = data.getString("added_on");
        editConfirm = (Button)view.findViewById(R.id.editConfirm);
        if(!(current_userId.toString().equals(user_id.toString()))){
            edit.setVisibility(Button.GONE);
            delete.setVisibility(Button.GONE);
        }
        else{
            et_name.setText(name);
            et_model.setText(model);
            tx_serialEdit.setText(serial);
            et_type.setText(type);
        }
        tx_productName.append(": "+name);
        tx_model.append(": "+model);
        tx_type.append(": "+type);
        tx_serialID.append(": "+serial);
        tx_added_on.append(": "+added_on);
        tx_productTitle.setText(name);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewer.setVisibility(LinearLayout.GONE);
                editer.setVisibility(LinearLayout.VISIBLE);
            }
        });

        editConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name, model, type, serialid;
                name = et_name.getText().toString().trim();
                model = et_model.getText().toString().trim();
                type = et_type.getText().toString().trim();
                serialid = tx_serialEdit.getText().toString().trim();
                if(!(name.isEmpty() || model.isEmpty() || type.isEmpty() || serialid.isEmpty())){
                    userDetails.updateProduct(name, model, type, serialid);
                }
                else{
                    tx_error.setText("All fields are compulsory");
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.deleteProduct(tx_serialEdit.getText().toString());
            }
        });

        complainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddComplainProductFragment addComplainProductFragment = new AddComplainProductFragment();
                Bundle args = new Bundle();
                args.putString("name",name);
                args.putString("model",model);
                args.putString("type",type);
                args.putString("serial",serial);
                addComplainProductFragment.setArguments(args);
                /*final Context context = parent.getContext();
                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();*/
                FragmentManager fragmentManager = getActivity().getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, addComplainProductFragment).addToBackStack("addProductComplain").commit();
            }
        });
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails)activity;
        }
        catch (Exception e){}
    }


    public interface UserDetails{
        public void deleteProduct(String serialId);
        public void updateProduct(String name, String model, String type, String serialId);
    }
}
