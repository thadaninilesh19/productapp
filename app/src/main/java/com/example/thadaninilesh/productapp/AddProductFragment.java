package com.example.thadaninilesh.productapp;


import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddProductFragment extends Fragment {

    UserDetails userDetails;
    EditText et_name, et_type, et_model, et_serialId;
    Button addProduct;
    TextView tv_error;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_product, container, false);
        et_name = (EditText)view.findViewById(R.id.name);
        et_model = (EditText)view.findViewById(R.id.model);
        et_type = (EditText)view.findViewById(R.id.type);
        et_serialId = (EditText)view.findViewById(R.id.serialId);
        tv_error = (TextView)view.findViewById(R.id.errorMessage);
        addProduct = (Button)view.findViewById(R.id.addProduct);
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name, model, type, serialId;
                name = et_name.getText().toString().trim();
                model = et_model.getText().toString().trim();
                type = et_type.getText().toString().trim();
                serialId = et_serialId.getText().toString().trim();

                if(name.isEmpty() || model.isEmpty() || type.isEmpty() || serialId.isEmpty()){
                    tv_error.setText("All fields are compulsory");
                }
                else{
                    userDetails.addProduct(name, type, model, serialId);
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails)activity;
        }
        catch (Exception e){}
    }

    public interface UserDetails{
        public void addProduct(String name, String type, String model, String serialId);
    }

}
