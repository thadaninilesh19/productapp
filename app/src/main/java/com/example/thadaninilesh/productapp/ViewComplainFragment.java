package com.example.thadaninilesh.productapp;


import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ViewComplainFragment extends Fragment {
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    ListView complainList;
    Button addComplain;
    ViewComplainAdapter viewComplainAdapter;
    String jsonString="", response="";
    JSONObject jsonObject;
    JSONArray jsonArray;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_view_complain, container, false);
        //new BackgroundViewComplainTask().execute();
        BackgroundViewComplainTask backgroundViewComplainTask = new BackgroundViewComplainTask();
        backgroundViewComplainTask.execute();
        complainList = (ListView)view.findViewById(R.id.list);
        addComplain = (Button)view.findViewById(R.id.addComplain);
        addComplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new AddComplainFragment()).addToBackStack("complainFragment").commit();
            }
        });
        return view;
    }

    public void populateComplainList(String jsonString){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        jsonString = sharedPreferences.getString("complain", "");
        String productName, serial, complainType, complainMessage, added_on;
        viewComplainAdapter = new ViewComplainAdapter(this.getActivity(), R.layout.complain_layout);
        try {
            Log.d("json",jsonString);
            jsonObject = new JSONObject(jsonString);
            jsonArray = jsonObject.getJSONArray("server_response");
            int count = 0;
            ViewComplainsList viewComplainsList;

            while (count<jsonArray.length()){
                JSONObject JO = jsonArray.getJSONObject(count);
                productName = JO.getString("productName");
                serial = JO.getString("serial");
                complainType = JO.getString("complainType");
                complainMessage = JO.getString("complainMessage");
                added_on = JO.getString("added_on");
                viewComplainsList = new ViewComplainsList(complainMessage, complainType, productName, serial, added_on);
                viewComplainAdapter.add(viewComplainsList);
                count++;
            }
            complainList.setAdapter(viewComplainAdapter);
            viewComplainAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(jsonString.isEmpty()){
            Toast.makeText(getActivity(),"No Complains found",Toast.LENGTH_LONG).show();
            this.startActivity(new Intent(getActivity(), MainActivity.class));
        }
    }

    private class ViewComplainsList{
        private String productName, serial, complainType, complainMessage, added_on;

        public ViewComplainsList(String complainMessage, String complainType, String productName, String serial, String added_on) {
            this.complainMessage = complainMessage;
            this.complainType = complainType;
            this.productName = productName;
            this.serial = serial;
            this.added_on = added_on;
            Log.d("result",this.complainMessage);
        }

        public String getAdded_on() {
            return added_on;
        }

        public void setAdded_on(String added_on) {
            this.added_on = added_on;
        }

        public String getComplainMessage() {
            return complainMessage;
        }

        public void setComplainMessage(String complainMessage) {
            this.complainMessage = complainMessage;
        }

        public String getComplainType() {
            return complainType;
        }

        public void setComplainType(String complainType) {
            this.complainType = complainType;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }
    }

    private class ViewComplainAdapter extends ArrayAdapter{
        List list = new ArrayList();
        private int layout;

        public ViewComplainAdapter(Context context, int resource){
            super(context, resource);
            layout = resource;
        }

        public void add(ViewComplainsList object){
            super.add(object);
            list.add(object);
        }

        public int getCount(){
            return list.size();
        }
        public Object getItem(int position){
            return list.get(position);
        }
        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            View row;
            row = convertView;
            ViewComplainsHolder viewComplainsHolder;
            if(row==null){
                LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = layoutInflater.inflate(R.layout.complain_layout,parent,false);
                viewComplainsHolder = new ViewComplainsHolder();
                viewComplainsHolder.tx_productName = (TextView)row.findViewById(R.id.productName);
                viewComplainsHolder.tx_serial = (TextView)row.findViewById(R.id.serial);
                viewComplainsHolder.tx_complainType = (TextView)row.findViewById(R.id.complainType);
                viewComplainsHolder.tx_complainMessage = (TextView)row.findViewById(R.id.complainMessage);
                //viewProductsHolder.button = (Button)row.findViewById(R.id.list_item_btn);
                row.setTag(viewComplainsHolder);
            }
            else{
                viewComplainsHolder = (ViewComplainsHolder) row.getTag();
            }
            final ViewComplainsList viewComplainsList = (ViewComplainsList) this.getItem(position);
            viewComplainsHolder.tx_productName.setText("Product Name: "+viewComplainsList.getProductName());
            viewComplainsHolder.tx_serial.setText("Serial ID: "+viewComplainsList.getSerial());
            viewComplainsHolder.tx_complainType.setText("Complain Type: "+viewComplainsList.getComplainType());
            viewComplainsHolder.tx_complainMessage.setText("Message: "+viewComplainsList.getComplainMessage());
            /*viewComplainsHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewSelectedProductFragment viewSelectedProductFragment = new ViewSelectedProductFragment();
                    Bundle args = new Bundle();
                    args.putString("name",viewProductsList.getName());
                    args.putString("model",viewProductsList.getModel());
                    args.putString("type",viewProductsList.getType());
                    args.putString("serialId",viewProductsList.getSerialId());
                    args.putString("user_id",viewProductsList.getUser_id());
                    args.putString("added_on",viewProductsList.getAdded_on());
                    viewSelectedProductFragment.setArguments(args);
                    final Context context = parent.getContext();
                    FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main_activity_frame, viewSelectedProductFragment).addToBackStack("viewSelectedProduct").commit();
                }
            });*/
            return row;
        }

        class ViewComplainsHolder{
            TextView tx_productName, tx_serial, tx_complainType, tx_complainMessage;
            Button button;
        }
    }

    private class BackgroundViewComplainTask extends AsyncTask<String, Void,String>{

        ProgressDialog progressDialog = null;
        Activity ctx;
        String user_id, path;
        public BackgroundViewComplainTask() {
            ctx = getActivity();
            progressDialog = new ProgressDialog(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading Complains");
            progressDialog.setCancelable(false);
            progressDialog.show();
            path = ctx.getString(R.string.url);
            sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
            user_id = sharedPreferences.getString("user_id", "");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {
            path = path+"getComplain.php";
            try {
                URL url = new URL(path);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                String data = URLEncoder.encode("id","UTF-8") +"="+URLEncoder.encode(user_id,"UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }
                Log.d("response",response);
                ViewComplainFragment viewComplainFragment = new ViewComplainFragment();
                Bundle b = new Bundle();
                b.putString("response",response);
                viewComplainFragment.setArguments(b);
                SharedPreferences sharedPreferences1;
                sharedPreferences1 = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences1.edit();
                editor.putString("complain", response);
                editor.commit();
                //sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);


                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != "empty") {
                populateComplainList(result);
            }
            else{
                Toast.makeText(ctx, "No results", Toast.LENGTH_SHORT).show();
            }
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                progressDialog = null;
            }
        }
    }
}
