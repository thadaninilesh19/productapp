package com.example.thadaninilesh.productapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {
    EditText et_username, et_password;
    Button login, register, forgetPassword;
    TextView tv_error;
    String username, password;
    SharedPreferences sharedPreferences;
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("ProductApp", Context.MODE_PRIVATE);
        final String email = sharedPreferences.getString("email", "");
        super.onCreate(savedInstanceState);
        if(!email.isEmpty()){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
        }
        setContentView(R.layout.activity_login);

        et_username = (EditText)findViewById(R.id.username);
        et_password = (EditText)findViewById(R.id.password);
        tv_error = (TextView)findViewById(R.id.errorMessage);

        login = (Button)findViewById(R.id.login);
        forgetPassword = (Button)findViewById(R.id.forgetPassword);
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if(isInternetPresent) {
                    startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
                }
                else{
                    Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void userLogin(View view){
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            username = et_username.getText().toString().trim();
            password = et_password.getText().toString().trim();
            if (username.isEmpty() || password.isEmpty()) {
                tv_error.setText("Username and Password are compulsory fields");
            } else {
                Log.d("message", password + " username or password " + username);
                BackgroundUserLoginTask backgroundUserLoginTask = new BackgroundUserLoginTask(this);
                backgroundUserLoginTask.execute(username, password);
//                finish();
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    public void userRegister(View view){
        CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            finish();
        }
        else{
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }
}