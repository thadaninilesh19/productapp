package com.example.thadaninilesh.productapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BackgroundAddProductTask extends AsyncTask<String,Void,String> {
    Context ctx;
    AlertDialog alertDialog;
    ProgressDialog progressDialog = null;
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    BackgroundAddProductTask(Context ctx){
        this.ctx = ctx;
    }
    String path="";
    String user_id;
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        sharedPreferences = ctx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id", "");
        path = ctx.getString(R.string.url);
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setTitle("Signing Up");
        progressDialog.setMessage("Checking your details!");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        String result="";
        //Toast.makeText(ctx,path,Toast.LENGTH_LONG).show();

        path = path + "addProduct.php";
        String name, model, type, serialId;
        name = params[0];
        model = params[1];
        type = params[2];
        serialId = params[3];
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String added_on = sdf.format(c.getTime());
        try {
            URL url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            String data = URLEncoder.encode("name","UTF-8") +"="+URLEncoder.encode(name,"UTF-8")+"&"+
                    URLEncoder.encode("model","UTF-8") +"="+URLEncoder.encode(model,"UTF-8")+"&"+
                    URLEncoder.encode("type","UTF-8") +"="+URLEncoder.encode(type,"UTF-8")+"&"+
                    URLEncoder.encode("serialid","UTF-8") +"="+URLEncoder.encode(serialId,"UTF-8")+"&"+
                    URLEncoder.encode("user_id","UTF-8") +"="+URLEncoder.encode(user_id,"UTF-8")+"&"+
                    URLEncoder.encode("added_on","UTF-8") +"="+URLEncoder.encode(added_on,"UTF-8");

            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            os.close();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        Log.d("register",response);
        return response;
    }



    @Override
    protected void onProgressUpdate(Void... values){
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        if(progressDialog!=null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            progressDialog= null;
        }
        if(result.equals("success")){
            Toast.makeText(ctx,"Product added successfully",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }
        else if(result.equals("duplicate")){
            Toast.makeText(ctx,"Duplicate entry found for product serial id",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }
        else{
            Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }

    }
}