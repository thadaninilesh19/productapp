package com.example.thadaninilesh.productapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BackgroundUserEditTask extends AsyncTask<String,Void,String> {
    Context ctx;
    AlertDialog alertDialog;
    ProgressDialog progressDialog = null;
    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedpreferences;
    BackgroundUserEditTask(Context ctx){
        this.ctx = ctx;
    }
    String method="";
    String path="";
    String name, new_name, username, new_username, email, new_email;
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        path = ctx.getString(R.string.url);
        progressDialog = new ProgressDialog(ctx);
        //progressDialog.setTitle("Signing In");
        progressDialog.setMessage("Updating your account details! Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        String result="";

        path = path + "editUser.php";
        name = params[0];
        new_name = params[1];
        email = params[2];
        new_email = params[3];
        username = params[4];
        new_username = params[5];
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String updated_at = sdf.format(c.getTime());

        Log.d("update", username + " " + new_username);
        try {
            Log.d("response", response+"before in try");
            URL url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String data = URLEncoder.encode("old_username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" +
                    URLEncoder.encode("old_name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&" +
                    URLEncoder.encode("old_email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&" +
                    URLEncoder.encode("new_username", "UTF-8") + "=" + URLEncoder.encode(new_username, "UTF-8") + "&" +
                    URLEncoder.encode("new_email", "UTF-8") + "=" + URLEncoder.encode(new_email, "UTF-8") + "&" +
                    URLEncoder.encode("new_name", "UTF-8") + "=" + URLEncoder.encode(new_name, "UTF-8") + "&" +
                    URLEncoder.encode("updated_at", "UTF-8") + "=" + URLEncoder.encode(updated_at, "UTF-8");
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            Log.d("response", response+"before");
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }
            Log.d("response", response);
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }



    @Override
    protected void onProgressUpdate(Void... values){

    }

    @Override
    protected void onPostExecute(String result){
        if(progressDialog!=null){
            if(progressDialog.isShowing() && progressDialog!=null){
                progressDialog.dismiss();
            }
            progressDialog= null;
        }
        if(result.equals("duplicate")){
            Toast.makeText(ctx,"Similar account exists!",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }
        else if(result.toString().equals("success") || result=="success"){
            sharedpreferences = ctx.getSharedPreferences(MyPREFERENCES, ctx.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("username", new_username);
            editor.putString("email", new_email);
            editor.putString("name", new_name);
            editor.commit();
            Toast.makeText(ctx,"Updation success",Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(ctx,"Error occured! Please try again.",Toast.LENGTH_LONG).show();
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        }

    }
}
