package com.example.thadaninilesh.productapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.app.Fragment;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link UserDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static final String MyPREFERENCES = "ProductApp" ;
    SharedPreferences sharedPreferences;
    UserDetails userDetails;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Button editUser, editUserDetails;
    TextView tv_name, tv_username, tv_email, tv_error;
    EditText et_name, et_username, et_email;
    LinearLayout ll_view, ll_edit;
    String old_email, old_username, old_name, id;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserDetailsFragment newInstance(String param1, String param2) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        id = sharedPreferences.getString("user_id", "");
        old_email = sharedPreferences.getString("email", null);
        old_username = sharedPreferences.getString("username", null);
        old_name = sharedPreferences.getString("name",null);


        View view = inflater.inflate(R.layout.fragment_user_details, container, false);

        editUser = (Button)view.findViewById(R.id.editButton);
        editUserDetails = (Button)view.findViewById(R.id.editUserDetails);
        ll_view = (LinearLayout)view.findViewById(R.id.viewDetails);
        ll_edit = (LinearLayout)view.findViewById(R.id.editDetails);
        et_name = (EditText)view.findViewById(R.id.new_name);
        et_email = (EditText)view.findViewById(R.id.new_email);
        et_username = (EditText)view.findViewById(R.id.new_username);
        tv_error = (TextView)view.findViewById(R.id.errorMessage);
        tv_name = (TextView)view.findViewById(R.id.name);
        tv_email = (TextView)view.findViewById(R.id.email);
        tv_username = (TextView)view.findViewById(R.id.username);
        tv_name.setText(old_name);
        tv_email.setText(old_email);
        tv_username.setText(old_username);
        ActionBar actionBar = getActivity().getActionBar();
        //actionBar.setTitle(old_name);
        editUser.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ll_view.setVisibility(LinearLayout.GONE);
                et_name.setText(old_name);
                et_username.setText(old_username);
                et_email.setText(old_email);
                ll_edit.setVisibility(LinearLayout.VISIBLE);
            }
        });

        editUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_name, new_email, new_username, name, email, username;
                new_name = et_name.getText().toString().trim();
                new_email = et_email.getText().toString().trim();
                new_username = et_username.getText().toString().trim();
                name = tv_name.getText().toString().trim();
                email = tv_email.getText().toString().trim();
                username = tv_username.getText().toString().trim();
                if(!(new_name.isEmpty() || new_email.isEmpty() || new_username.isEmpty())){
                    if(new_name==old_name && new_email==old_email && old_username==new_username){
                        tv_error.setText("No changes were made to the profile");
                    }
                    else {
                        userDetails.updateUser(name, new_name, email, new_email, username, new_username);
                    }
                }
                else{
                    tv_error.setText("All fields are mandatory!");
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails)activity;
        }
        catch (Exception e){}
    }


    public interface UserDetails{
        public void updateUser(String name, String new_name, String email, String new_email, String username, String new_username);
    }

}
