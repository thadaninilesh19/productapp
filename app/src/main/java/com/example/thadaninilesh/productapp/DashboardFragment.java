package com.example.thadaninilesh.productapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DashboardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    Button addProduct, myProfile, changePassword, logout;
    UserDetails userDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        addProduct = (Button)view.findViewById(R.id.addProduct);
        myProfile = (Button)view.findViewById(R.id.myProfile);
        changePassword = (Button)view.findViewById(R.id.changePassword);
        logout = (Button)view.findViewById(R.id.logout);


        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.changeTitle("Add Product");
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new AddProductFragment()).addToBackStack("addProduct").commit();
            }
        });
        myProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.changeTitle("Edit Details");
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new UserDetailsFragment()).addToBackStack("editDetails").commit();
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.changeTitle("Change Password");
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_activity_frame, new ChangePasswordFragment()).addToBackStack("changePassword").commit();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDetails.changeTitle("Logout");
                BackgroundUserLogoutTask backgroundUserLogoutTask = new BackgroundUserLogoutTask(getActivity());
                backgroundUserLogoutTask.execute();
                //getActivity().finish();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userDetails = (UserDetails)activity;
        }
        catch (Exception e){}
    }

    public interface UserDetails {
        public void changeTitle(String title);
    }
}
