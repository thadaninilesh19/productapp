package com.example.thadaninilesh.productapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SplashScreenActivity extends Activity {
    SharedPreferences sharedPreferences;
    private static int SPLASH_TIME_OUT = 1500;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        textView = (TextView)findViewById(R.id.tt);
        /*======================================================================*/
        sharedPreferences = getSharedPreferences("ProductApp", Context.MODE_PRIVATE);
        final String email = sharedPreferences.getString("email", "");
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                //textView.setText(email);
                if(email.isEmpty()){
                    startActivity(new Intent(SplashScreenActivity.this,LoginActivity.class));
                }
                else{
                    startActivity(new Intent(SplashScreenActivity.this,MainActivity.class));
                }
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
